<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">

    <title>Locate, Buy & Sell  for Free</title>
    <meta name="description" content="">
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Locate, Buy & Sell Online for free"/>
    <meta property="og:image" content="/images/logofirst.jpg"/>
    <meta property="og:description" content="Marketlinkus is your Marketplace to Locate, Buy & Sell online everything that you need for free."/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Favicon and Touch Icons-->
    <link href="/images/logofirst.jpg" rel="icon">
    <link href="/images/logofirst.jpg" rel="apple-touch-icon">

    <link rel="manifest" href="site.webmanifest">
    <link rel="mask-icon" color="#fe6a6a" href="safari-pinned-tab.svg">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!-- Vendor Styles including: Font Icons, Plugins, etc.-->
    <link rel="stylesheet" media="screen" href="/assets/vendor/simplebar/dist/simplebar.min.css"/>
    <link rel="stylesheet" media="screen" href="/assets/vendor/tiny-slider/dist/tiny-slider.css"/>
    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" href="/assets/css/theme.css">
    <link rel="stylesheet" media="screen" href="/assets/css/style.css">
        <link rel="stylesheet" media="screen" href="/css/invoice.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <!-- Body-->
  <body class="bg-secondary">
       
       <div id="app">
        <app></app>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>

<!-- Back To Top Button-->
{{-- <a class="btn-scroll-top" href="home-grocery-store.html#top" data-scroll data-fixed-element><span class="btn-scroll-top-tooltip text-muted fs-sm me-2">Top</span><i class="btn-scroll-top-icon ci-arrow-up">   </i></a> --}}
    <!-- Vendor scrits: js libraries and plugins-->
    <script src="/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/vendor/simplebar/dist/simplebar.min.js"></script>
    <script src="/assets/vendor/tiny-slider/dist/min/tiny-slider.js"></script>
    <script src="/assets/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
    <!-- Main theme script-->
    <script src="/assets/js/theme.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
    <script src="/loader/center-loader.js"></script>

    <script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
    </body>
</html>
